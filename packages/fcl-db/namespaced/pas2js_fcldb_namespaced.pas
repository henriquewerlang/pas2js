{ This file was automatically created by Lazarus. Do not edit!
  This source is only used to compile and install the package.
 }

unit pas2js_fcldb_namespaced;

{$warn 5023 off : no warning about unused units}
interface

uses
  Data.Db, Data.Consts, Data.JsonDataset, Data.Rest.Connection, Data.ExtJS, Data.FieldMap, Data.HTML.Actions, Data.LocalDataset, 
  Data.Rest.SqlDb;

implementation

end.
