{ This file was automatically created by Lazarus. Do not edit!
  This source is only used to compile and install the package.
 }

unit pas2js_rtl_namespaced;

{$warn 5023 off : no warning about unused units}
interface

uses
  System.Classes, JSApi.JS, System.Math, NodeApi.JS, ObjPas, System.RTLConsts, System.SysUtils, System.Types, System.TypInfo, 
  BrowserApi.Web, System.HotReload, System.DateUtils, System.StrUtils, System.Contnrs, Browser.Console, System.Router, 
  System.Rtti, BrowserApi.WebGL, System.Class2Pas, Api.JQuery, System.Timer, BrowserApi.Audio, BrowserApi.BlueTooth, 
  BrowserApi.SVG, Browser.ScriptLoader, Browser.TemplateLoader, Browser.UnitLoader, BrowserApi.RTC, BrowserApi.WebAssembly, 
  BrowserApi.WebOrWorker, BrowserApi.Worker, BrowserApi.LoadHelper, HTML.Actions, HTML.EventNames, HTML.Translate, HTML.Utils, 
  System.ArrayUtils, System.Generics.Collections, System.Generics.Defaults, System.Generics.Strings, System.Res, 
  System.ResourceTranslate, System.SimpleLinkedList, System.WebUtils;

implementation

end.
