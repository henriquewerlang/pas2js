# Webassembly HTTP api demo.

This demo shows how to do HTTP requests from Webassembly. 
Because HTTP requests in the browser are asynchronous, the API is also
asynchronous.

To make this demo work, you need to compile the wasmhttpdemo.pp example
project that comes with fpc (in `packages/wasm-utils/demos/http`), and put
it in this demo directory.

